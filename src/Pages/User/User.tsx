import { SubmitHandler, useForm } from "react-hook-form";
import {  useNavigate } from "react-router-dom";
import Header from "../../Components/Header/Header";
import { IUser } from "./User.type";
import styles from "./User.module.scss"
import { useContext } from "react";
import { AppContext } from "../../Components/Layout/Layout";
import { enterMarks } from "../../Services/marks.service";
import { getMarks } from "../../Services/marks.service";
import UserForm from "../../Components/UserForm/UserForm";

const User = () => {

    const {register,reset,formState:{errors} ,handleSubmit } = useForm<IUser>();
    const {setRow,email}=useContext(AppContext);
    const navigate=useNavigate();

    
    const onView=async()=>{
        const rowData=await getMarks(email);
        navigate("/user/userMarks");
        setRow(rowData);
    }
    return (
        <div className={styles.main}>
            <Header
                text="User Home Page" />
            <div className={styles.viewContainer}>
           
                <button className={styles.viewBtn} onClick={onView} >View</button>
            </div>
            <div className={styles.formContainer}>
                {/* <form onSubmit={handleSubmit(onSubmit)}>
                <div className={styles.formHeading}>
                        <h3>Enter subject Marks</h3>
                    </div>
                    <div className={styles.inputContainer}>
                        <input 
                        type="number" 
                        placeholder="Mathematics"
                        className={styles.input}
                        {...register("mathematics", {
                            min:0,
                            max:100
                          })} />
                    </div>
                    <div className={styles.inputContainer}>
                        <input 
                        type="number" 
                        placeholder="Science"
                        className={styles.input}
                        {...register("science", {
                            min:0,
                            max:100
                          })} />
                    </div>
                    <div className={styles.inputContainer}>
                        <input 
                        type="number" 
                        placeholder="Geography"
                        className={styles.input} 
                        {...register("geography", {
                            min:0,
                            max:100
                          })}/>
                    </div>
                    <div className={styles.inputContainer}>
                        <input 
                        type="number" 
                        placeholder="History"
                        className={styles.input} 
                        {...register("history", {
                            min:0,
                            max:100
                          })}/>
                    </div>

                    <div className={styles.btnContainer}>
                        <button type="submit" >
                            Submit
                        </button>

                    </div>

                </form> */}
                <UserForm/>
            </div>
        </div>
    )
}
export default User;