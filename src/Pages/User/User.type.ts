export interface IUser {
  mathematics: number;
  history: number;
  science: number;
  geography: number;
}
