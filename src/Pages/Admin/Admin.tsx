import { SubmitHandler, useForm } from "react-hook-form";
import Header from "../../Components/Header/Header";
import { IAdmin } from "./Admin.type";
import styles from "./Admin.module.scss";
import { createUser } from "../../Services/login.service";

const Admin=()=>{
    const { register, reset, formState: { errors }, handleSubmit } = useForm<IAdmin>();

    const onSubmit:SubmitHandler<IAdmin>=(data)=>{
        const dataToSend={
            email:data.email,
            password:data.password,
            username:data.name
        };
        createUser(dataToSend);
        reset();
    }
    return(
        <div >
            <Header
            text="Admin"/>
            <div className={styles.formContainer}>
                <form onSubmit={handleSubmit(onSubmit)}>
                <div className={styles.formHeading}>
                        <h3>Enter User Details</h3>
                    </div>
                <div className={styles.inputContainer}>
                    <input
                    type="text"
                    placeholder="Enter Name"
                    className={styles.input}
                    {...register("name", {
                        required: { value: true, message: "Enter name" },
                      })}        
                    />
                    {errors.name?.type==="required"&&(
                        <span>{errors.name.message}</span>
                    )}
                </div>
                <div className={styles.inputContainer}>
                    
                    <input
                    type="email"
                    placeholder="Enter user Email-Id"
                    className={styles.input}
                    {...register("email", {
                        required: { value: true, message: "Enter email" },
                      })} />
                     {errors.email?.type==="required"&&(
                        <span>{errors.email.message}</span>
                    )}
                </div>
                <div className={styles.inputContainer}>
                    <input
                    type="password"
                    placeholder="Enter user password"
                    className={styles.input}
                    {...register("password", {
                        required: { value: true, message: "Enter password" },
                        minLength:{value:8,message:"Min lenght should be 8"}
                      })}/>
                      {errors.password?.type==="minLenght"&&(
                        <span>{errors.password?.message}</span>
                    )}
                      {errors.password?.type==="required"&&(
                        <span>{errors.password.message}</span>
                    )}
                    
                    </div>    
                    
                    <div className={styles.btnContainer}>
                    <button
                    type="submit">Submit</button>
                    </div>
                    
                </form>
            </div>
        </div>
    )
}

export default Admin;