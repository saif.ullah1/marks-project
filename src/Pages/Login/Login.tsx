import Form from "../../Components/Form/Form";
import Header from "../../Components/Header/Header";

const Login = () => {
    return (
        <div>
            <Header
                text="Login" />
            <Form />
        </div>

    )
}

export default Login;