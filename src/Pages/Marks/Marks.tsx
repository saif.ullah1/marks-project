import { DataGrid, GridColDef } from "@mui/x-data-grid";
import { useContext } from "react";
import Header from "../../Components/Header/Header";
import { AppContext } from "../../Components/Layout/Layout";
import styles from"./Marks.module.scss";
const Marks = () => {
  const { row } = useContext(AppContext);
  const columns: GridColDef[] = [
    {
      field: 'id',
      headerName: 'ID',
      type: 'number',
      width: 50
    },
    {
      field: "mathematics",
      headerName: "Mathematics",
      width: 110,
      type: 'number',

    },
    {
      field: "history",
      headerName: "History",
      width: 110,
      type: 'number',

    },
    {
      field: "geography",
      headerName: "Geography",
      width: 110,
      type: 'number',


    },
    {
      field: "science",
      headerName: "Science",
      width: 110,
      type: 'number',

    },
    {
      field: 'date',
      width: 110,
      headerName: "Date",
    }

  ];
  const rows=[];
  for(let data of row){
    const score={
      id:data.id,
      mathematics: data.mathsScore,
      history: data.historyScore,
      geography: data.geograpgyScore,
      science: data.scienceScore,
      date: data.date.slice(0,10)
    }
    rows.push(score);
  }


  return (
    <div className={styles.main}>
      <Header
      text="Marks of user"/>
      <div className={styles.grid} >
      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={2}
        rowsPerPageOptions={[2,4,8,10]}
      />
    </div>
    </div>
    
  )
}
export default Marks;