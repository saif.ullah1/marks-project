
export interface IRows {
    id: number,
    mathsScore: number,
    scienceScore: number,
    historyScore: number,
    geograpgyScore : number,
    
    date: string
}

export type IRow=IRows[];