export interface Imarks{
    mailId:string,
    mathsScore:number,
    sciScore:number,
    histScore:number,
    geoScore:number
}