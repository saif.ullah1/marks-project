import { useContext } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { IUser } from "../../Pages/User/User.type";
import { enterMarks } from "../../Services/marks.service";
import { AppContext } from "../Layout/Layout";
import styles from "./UserForm.module.scss"
const UserForm=()=>{
    const {register,reset,formState:{errors} ,handleSubmit } = useForm<IUser>();
    const{email}=useContext(AppContext);
    const onSubmit:SubmitHandler<IUser> = (data) => {
        const dataToSend={
            mailId:email,
            mathsScore: data.mathematics,
            sciScore: data.science,
            histScore: data.history,
            geoScore: data.geography
            
        }
        enterMarks(dataToSend);
        reset();
    };
    return(
        <form onSubmit={handleSubmit(onSubmit)}>
                <div className={styles.formHeading}>
                        <h3>Enter subject Marks</h3>
                    </div>
                    <div className={styles.inputContainer}>
                        <input 
                        type="number" 
                        placeholder="Mathematics"
                        className={styles.input}
                        {...register("mathematics", {
                            min:0,
                            max:100
                          })} />
                    </div>
                    <div className={styles.inputContainer}>
                        <input 
                        type="number" 
                        placeholder="Science"
                        className={styles.input}
                        {...register("science", {
                            min:0,
                            max:100
                          })} />
                    </div>
                    <div className={styles.inputContainer}>
                        <input 
                        type="number" 
                        placeholder="Geography"
                        className={styles.input} 
                        {...register("geography", {
                            min:0,
                            max:100
                          })}/>
                    </div>
                    <div className={styles.inputContainer}>
                        <input 
                        type="number" 
                        placeholder="History"
                        className={styles.input} 
                        {...register("history", {
                            min:0,
                            max:100
                          })}/>
                    </div>

                    <div className={styles.btnContainer}>
                        <button type="submit" >
                            Submit
                        </button>

                    </div>

                </form>
    )
}

export default UserForm;



