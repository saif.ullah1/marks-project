// import styles from "./Layout.module.scss";
import { Routes, Route } from 'react-router-dom';
import Login from "../../Pages/Login/Login";
import Admin from "../../Pages/Admin/Admin";
import User from "../../Pages/User/User";
import Marks from "../../Pages/Marks/Marks";
import { createContext, useState } from 'react';
import { IRow, IRows } from '../../Pages/Marks/Marks.type';

export const AppContext = createContext({
    email: "",
    setEmail: (e: string) => { },
    row: [
        {
            id: 0,
            mathsScore: 0,
            scienceScore: 0,
            geograpgyScore: 0,
            historyScore: 0,
           date: "",
        },
    ],
    setRow: (r: IRow) => { }
});
const Layout = () => {
    const [email, setEmail] = useState<string>("");
    const [row, setRow] = useState<IRows[]>([
        {
            id: 0,
            mathsScore: 0,
            scienceScore: 0,
            geograpgyScore: 0,
            historyScore: 0,
            date: "",

        }
    ]);

    return (
        <div>

            <main>
                <AppContext.Provider value={{ email, setEmail, row, setRow }}>
                    <Routes>
                        <Route path='/' >
                            <Route index element={<Login />} />
                            <Route path='user'>
                                <Route index element={<User />} />
                                <Route path=':userMarks' element={<Marks />} />
                            </Route>
                            <Route path='admin' element={<Admin />} />
                        </Route>
                    </Routes>
                </AppContext.Provider>
            </main>

        </div>

    )
}


export default Layout;