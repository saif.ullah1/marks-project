import { IHeader } from "./Header.type";
import styles from "./Header.module.scss"

const Header = ({ text }: IHeader) => {
    return (
        <header className={styles.header}>{text}</header>
    )
}

export default Header;