import { SubmitHandler, useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { Iform } from './Form.type';
import styles from "./Form.module.scss"
import { useContext, useState } from 'react';
import { AppContext } from '../Layout/Layout';
import { loginRole } from '../../Services/login.service';

const Form = () => {
    const navigate = useNavigate();
    const [validation, setValidation] = useState<string>();
    const { setEmail } = useContext(AppContext);
    const { register, reset, formState: { errors }, handleSubmit } = useForm<Iform>();
    const onSubmit: SubmitHandler<Iform> = async (data) => {
        const dataBody = {
            Email: data.email,
            Password: data.password,
        };
        const role = await loginRole(dataBody);
        role === "Admin"
            ? navigate("/admin")
            : role === "User"
                ? navigate("/user")
                : setValidation("Register User");

        if (!validation) {
            setEmail(data.email);

        }

        reset();
    };


    return (
        <div className={styles.formContainer}>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className={styles.inputContainer}>
                    <input
                        type="email"
                        className={styles.input}
                        {...register("email", {
                            required: { value: true, message: "enter email id" }
                        })}
                        placeholder="user name"
                    />
                    {errors.email?.type === "required" && (
                        <span>{errors.email.message}</span>
                    )}
                </div>
                <div className={styles.inputContainer}>
                    <input
                        type="password"
                        {...register("password", {
                            required: { value: true, message: "Enter password" },
                            minLength: {
                                value: 8,
                                message: "Password length should be min 8 characters"
                            }
                        })}
                        placeholder="Password"
                        className={styles.input}
                    />
                    {errors.password?.type === "required" && (
                        <span>{errors.password.message}</span>
                    )}
                    {errors.password?.type === "minLenght" && (
                        <span>{errors.password?.message}</span>
                    )}
                </div>


                <div className={styles.btnContainer}>
                    <button type="submit" >
                        Submit
                    </button>

                </div>

            </form>
        </div>

    );
}

export default Form;